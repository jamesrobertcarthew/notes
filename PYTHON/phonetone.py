#!/user/bin/env python

import argparse
import os

# A lame/quick way of making ringtones 
# based of Australian mobile phone numbers
# needs fleshing out

parser = argparse.ArgumentParser(description='a script to make ring tones from phone numbers')

parser.add_argument('number', metavar='N', type=str, nargs='+',
                    help='A mobile phone number: +61XXXXXXXXX')
args = vars(parser.parse_args())


class ToneComponent(object):
    def __init__(self, number, style):
        self.number = number
        self.style = style
        values = []
        for i in number[3:]:
            if int(i) != 0:
                values.append(i)
        while len(values) < 7:
            values.append(values)
        # INTERVALS N N YES
        if int(values[1]) != 1:
            self.interval_1 = int(values[1])
        else:
            # GO HERE FOR MORE INFO http://thedailywtf.com/articles/when-you-really-want-to-be-sure-it-s-an-int
            self.interval_1 = int(values[2]) + int(values[1])
        if int(values[2]) != int(values[1]) and int(values[2]) != 1:
            self.interval_2 = int(values[2])
        elif int(values[2]) + int(values[3]) != self.interval_1:
            self.interval_2 = int(values[2]) + int(values[3])
        else:
            self.interval_2 = int(values[2]) + int(values[3]) + int(values[1])
        # ITERATION -i YES
        self.iteration = 0
        i = 1
        while self.iteration < 4:
            self.iteration += int(values[i])
            i += 1
        if self.iteration > 8:
            self.iteration = int(self.iteration/2)
        # TONALITY -t YES
        if int(values[4]) < 5.5:
            self.tonality = 'major'
        else:
            self.tonality = 'minor'
        if self.style == 'drums':
            self.tonality = self.style
        # FIRST NOTE -f YES
        self.first_note = 0
        i = 7
        while self.first_note > 0 and self.first_note < 7:
            self.first_note += int(values[i])
            i -= 1
        # LOOP LENGTH -l YES
        self.loop_length = int(values[6])
        if self.loop_length < 4:
            self.loop_length += int(values[5])
        # KEY -k YES
        self.key = 0
        i = 5
        while self.first_note > 0 and self.first_note < 7:
            self.first_note += int(values[i])
            i -= 1

    def make_command_string(self):
        if self.style != 'drums':
            cmd_string = 'python notes.py {!s} {!s} -i {!s} -l {!s} -f {!s} -k  {!s} -t  {!s} -o {!s}'.format(
                self.interval_1, self.interval_2, self.iteration, 4, 1, self.key,
                self.tonality, (self.number+'/'+self.style+self.number)
            )
        else:
            cmd_string = 'python notes.py {!s} {!s} -i {!s} -l {!s} -t {!s} -o {!s}'.format(
                self.interval_1, self.interval_2, self.iteration, self.loop_length, self.tonality,
                (self.number+'/'+self.style+self.number)
            )
        return cmd_string


def main():

    number = (args['number'][0])
    print "Constructing ring tone for {!s}" .format(number)
    if len(number) != 12:
        print "I don't know what to do with {!s}".format(number)
        return

    melody = ToneComponent(number, 'melody')
    melody_cmd_string = melody.make_command_string()
    drums = ToneComponent(number, 'drums')
    drums_cmd_string = drums.make_command_string()
    print drums_cmd_string
    print melody_cmd_string

    os.system('mkdir {!s}'.format(number))
    os.system(melody_cmd_string)
    os.system(drums_cmd_string)




if __name__ == "__main__":
    main()